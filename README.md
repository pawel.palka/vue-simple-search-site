# vue-simple-search-site

> Example app in vue.js, its used https://github.com/pespantelis/vue-typeahead/ for autosuggestion

## Build Setup

``` bash
git clone git@gitlab.com:pawel.palka/vue-simple-search-site.git
cd vue-simple-search-site

# install dependencies
npm install

# serve with hot reload 
npm run dev
Navigate to http://localhost:8080/

# build for production with minification
npm run build

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

# get data provider
wiki data provider: https://gitlab.com/pawel.palka/wiki-autosuggestion/blob/master/README.md


